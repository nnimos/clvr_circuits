﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{

    public GameObject AddedObject;
    public GameObject ResistorPrefab;
    public GameObject BatteryPrefab;
    public CircuitElement ElementScript;
    public GameObject ElementCircuitBoard;
    public bool IsAddingElement = false;
    public CircuitBoard TestBoard;
   

    Vector2 mousePos = new Vector2();


    // Wire variables
    public bool IsAddingWire = false;
    public bool FirstClick = false;
    public GameObject WirePrefab;
    public LineRenderer WireLineRenderer;

    public void AddResistor()
    {
        AddedObject = Instantiate(ResistorPrefab, ElementCircuitBoard.transform);
        ElementScript = AddedObject.GetComponent<CircuitElement>();
        IsAddingElement = true;
    }

    public void AddBattery(){
        AddedObject = Instantiate(BatteryPrefab, ElementCircuitBoard.transform);
        ElementScript = AddedObject.GetComponent<CircuitElement>();
        IsAddingElement = true;
    }

    public void AddWire(){
        AddedObject = Instantiate(WirePrefab, ElementCircuitBoard.transform);
        ElementScript = AddedObject.GetComponent<CircuitElement>();
        WireLineRenderer = AddedObject.GetComponent<LineRenderer>();
        WireLineRenderer.enabled = false;
        IsAddingWire = true;
    }

    public void OutputNetlist(){
        NetlistCreator.CreateNetlist(TestBoard);
    }

    void Update()
    {
        Vector3 MouseWorldPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));
        if (IsAddingElement)
        {
            if (Input.GetMouseButtonDown(0))
            {
                // Stop adding element
                ElementScript.OnObjectRelease();
                IsAddingElement = false;
                return;
            }
            else if(Input.GetKeyDown("r")){
                AddedObject.transform.eulerAngles += new Vector3(0, 90, 0);
            }
            // Move the object around
            AddedObject.transform.position = new Vector3(MouseWorldPosition.x, AddedObject.transform.position.y, MouseWorldPosition.z);

        }
        else if(IsAddingWire){

            if (Input.GetMouseButtonDown(0))
            {
                if(!FirstClick){
                    // Place on first node
                    FirstClick = true;
                    WireLineRenderer.enabled = true;
                    return;
                }
                else{
                    // Second node
                    FirstClick = false;
                    IsAddingWire = false;
                    if (ElementScript.ElementEnds[1].IsTouchingPort){
                        WireLineRenderer.SetPosition(1, ElementScript.ElementEnds[1].CollidingPort.transform.position);
                        ElementScript.WireJoin();
                    }
                    else{
                        Destroy(AddedObject);
                    }
                    return;
                }
            }
            else if(FirstClick)
            {
                // Update the line location to the mouse position
                // Move the object around
               
                ElementScript.ElementEnds[1].transform.position = new Vector3(MouseWorldPosition.x, AddedObject.transform.position.y, MouseWorldPosition.z);
                if (ElementScript.ElementEnds[1].IsTouchingPort)
                {
                    WireLineRenderer.SetPosition(1, ElementScript.ElementEnds[1].CollidingPort.transform.position);
                }
                else
                {
                    WireLineRenderer.SetPosition(1, new Vector3(MouseWorldPosition.x, AddedObject.transform.position.y, MouseWorldPosition.z));
                }
            }
            else{
                // Move the first end
                ElementScript.ElementEnds[0].transform.position = new Vector3(MouseWorldPosition.x, AddedObject.transform.position.y, MouseWorldPosition.z);
                if(ElementScript.ElementEnds[0].IsTouchingPort){
                    WireLineRenderer.SetPosition(0, ElementScript.ElementEnds[0].CollidingPort.transform.position);
                }
                else{
                    WireLineRenderer.enabled = false;
                }
            }
        }
    }

}
