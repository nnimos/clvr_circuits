﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CircuitNode : MonoBehaviour
{
    // Element properties
    public CircuitElementPort[] NodeElementPorts;
    [SerializeField]
    public List<CircuitElementPort> ConnectedElements; // Elements connected to the node

    // Node properies
    public CircuitBoard CircuitBoardScript;
    [SerializeField]
    public int NodeNumber; // Number of the node
    public float Voltage; // Voltage of the node
    public bool IsNodeActive; // Has the node been activated
    public bool IsConnectedToAnotherNode = false;
    public CircuitNode ConnectedNode;
    public bool IsGrounded = false;

    public CircuitNode()
    {

        IsNodeActive = false; // Make node inactive
        ConnectedElements = new List<CircuitElementPort>(); // Initialise connected elements array
        Voltage = 0; // Initialise the voltage to 0
    }

    public void Start()
    {
        CircuitBoardScript = this.transform.parent.GetComponent<CircuitBoard>();
    }
    // Gets the voltage of the node
    public float GetVoltage()
    {
        return Voltage;
    }

    // Adds an element to the node
    public void AddElement(CircuitElementPort tmp_Element)
    {
        if (!IsNodeActive)
        {
            ActivateNode();
                ConnectedElements.Add(tmp_Element);
        }
        else
        {
            ConnectedElements.Add(tmp_Element);
        }
    }

    // Removes an element from the node
    public void RemoveElement(CircuitElementPort tmp_Element)
    {
        if (IsNodeActive)
        {
            if (ConnectedElements.Contains(tmp_Element))
            {
                ConnectedElements.Remove(tmp_Element);
            }

            if (ConnectedElements.Count == 0)
            {
                DeactivateNode();
            }
        }
    }

    // Connects the node to the circuit
    public void ActivateNode()
    {
        IsNodeActive = true;
    }

    // Removes the node from the circuit
    public void DeactivateNode()
    {
        IsNodeActive = false;
    }

}
