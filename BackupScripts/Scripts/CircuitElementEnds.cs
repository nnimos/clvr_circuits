﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircuitElementEnds : MonoBehaviour
{
    public bool IsTouchingPort = false;
    public CircuitElementPort CollidingPort;

    void OnTriggerEnter(Collider other)
    {
       // Debug.Log(this.name + " " + other.name);
        IsTouchingPort = true;
        CollidingPort = other.gameObject.GetComponent<CircuitElementPort>();
    }

    void OnTriggerExit(Collider other)
    {
        //Debug.Log(this.name + " " + other.name);
        //IsTouchingPort = false;
        //CollidingPort = null;
    }
}
