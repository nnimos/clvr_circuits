﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetlistCreator : MonoBehaviour
{
    static string ServerAddress = "http://52.255.36.53/Simulation.php";
    /* Example netlist
	 
    * Circuit.cir
    v1 n0 0 dc 25 
    v2 n2 0 dc 15 
    r1 n0 n1 20k
    r2 n1 n2 10k 
    r3 n1 0 4.7k

    .control
    op
    option numdgt=2
    set wr_singlescale
    set noprintscale
    let loopnumber = 1
    let nodenumber = 3
    print n0 > /var/www/scripts/circuitresult.txt
    set appendwrite
    while loopnumber < nodenumber
        print n$&loopnumber >> /var/www/scripts/circuitresult.txt
        let loopnumber = loopnumber + 1
    end
    .endc
    .end
    
    */

    public static void CreateNetlist(CircuitBoard Board)
    {
        string FinishedString = "* Circuit.cir\r\n";
        foreach (CircuitElement tmp_Element in Board.AddedElements)
        {
            if (tmp_Element.ElementType == 0)
            {
                // Ground

            }
            else
            {
                // Element
                FinishedString = FinishedString + AddElement(tmp_Element.ElementType, tmp_Element.ElementEnds[0].CollidingPort, tmp_Element.ElementEnds[1].CollidingPort, tmp_Element.ElementValue, 0, Board);
            }


        }
        //FinishedString += AddOptions(CircuitSim.NodeList);
        //endRequest(FinishedString);
        Debug.Log(FinishedString);
    }

    static string AddElement(int Type, CircuitElementPort PortPositive, CircuitElementPort PortNegative, float Value, int ElementNumber, CircuitBoard Board)
    {
        CircuitNode NodeNumber1;
        CircuitNode NodeNumber2;

        // Check for merged nodes in the positive node
        if (PortPositive.ParentNode.IsConnectedToAnotherNode)
        {
            // Return the connected node
            if (Board.NodeArray.Contains(PortPositive.ParentNode))
            {
                // The current node is the "parent" node that the other node has merged into
                NodeNumber1 = PortPositive.ParentNode;
            }
            else
            {
                // The other node is the parent node
                NodeNumber1 = PortPositive.ParentNode.ConnectedNode;
            }
        }
        else
        {
            // The current node is the parent node
            NodeNumber1 = PortPositive.ParentNode;
        }

        // Check for merged nodes in the negative node
        if (PortNegative.ParentNode.IsConnectedToAnotherNode)
        {
            // Return the connected node
            if (Board.NodeArray.Contains(PortNegative.ParentNode))
            {
                // The current node is the "parent" node that the other node has merged into
                NodeNumber2 = PortNegative.ParentNode;
            }
            else
            {
                // The other node is the parent node
                NodeNumber2 = PortNegative.ParentNode.ConnectedNode;
            }
        }
        else
        {
            // The current node is the parent node
            NodeNumber2 = PortNegative.ParentNode;
        }

        if (NodeNumber1.IsGrounded)
        {
            return string.Format("{0} 0 n{1} {2}\r\n", CircuitManager.ElementDictionary[Type] + ElementNumber, NodeNumber2.NodeNumber, Value);
        }
        else if (NodeNumber2.IsGrounded)
        {
            return string.Format("{0} n{1} 0 {2}\r\n", CircuitManager.ElementDictionary[Type] + ElementNumber, NodeNumber1.NodeNumber, Value);
        }
        else
        {
            return string.Format("{0} n{1} n{2} {3}\r\n", CircuitManager.ElementDictionary[Type] + ElementNumber, NodeNumber1.NodeNumber, NodeNumber2.NodeNumber, Value);
        }

    }

    /*
	public void CreateNetlist()
	{
		string FinishedString = "* Circuit.cir\r\n";
		foreach(Element tmp_Element in CircuitSim.ElementList)
		{
			if(!tmp_Element.IsGround && !tmp_Element.IsWire){
				FinishedString = FinishedString + AddElement(tmp_Element.Type, tmp_Element.NodeList[0], tmp_Element.NodeList[1], tmp_Element.Value, tmp_Element.ElementNumber);
			}
			else if(tmp_Element.IsWire){
				
			}

		}
		FinishedString += AddOptions(CircuitSim.NodeList);
		SendRequest(FinishedString);
		Debug.Log(FinishedString);
	}
    
	string AddOptions(List<Node> NodeList)
	{
		string Options = ".control\r\n op\r\n option numdgt = 2\r\n set wr_singlescale\r\n set noprintscale\r\n";
		bool IsFirstRun = true;
		foreach(Node tmp_Node in NodeList){
			
			if(!tmp_Node.IsGround){
				if (IsFirstRun)
                {
                    IsFirstRun = false;
					Options += string.Format("print n{0} > /var/www/scripts/circuitresult.txt\r\nset appendwrite\r\n", tmp_Node.NodeNumber);
                }
                else
                {
					Options += string.Format("print n{0} >> /var/www/scripts/circuitresult.txt\r\n", tmp_Node.NodeNumber);
                }
			}         
		}

		Options+= ".endc\r\n.end";

		return Options;
	}



	void SendRequest(string NetList){
		StartCoroutine(RequestNodes(NetList));
	}

	IEnumerator RequestNodes(string NetList)
    {
		WWWForm form = new WWWForm();
		form.AddField("netlist", NetList); 
		UnityWebRequest www = UnityWebRequest.Post(ServerAddress, form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
			while(!www.isDone){
				yield return null;
			}
			Debug.Log(www.downloadHandler.text);
			//CircuitSim.UpdateNodeVoltages(www.downloadHandler.text);
        }
    */
}
