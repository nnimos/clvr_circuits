﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_EDITOR
public class CircuitBoardSetUp : EditorWindow
{
#else
public class CircuitBoardSetUp : MonoBehaviour
{
#endif

    public static Shader BuildingShader;
    public static Color BuildingColour;


#if UNITY_EDITOR
    [MenuItem("Assets/CircuitBoard set up")]
    public static void ShowMaterialChanger()
    {
#if UNITY_EDITOR
        GetWindow<CircuitBoardSetUp>(true, "Circuit board set up").Repaint();
#endif
    }


    void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Add scripts"))
        {
            AddScripts();
        }
        

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Fill in scripts"))
        {
            FillScripts();
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Remove all circuit scripts"))
        {
            //RemoveAllCircuitScripts();
        }
        EditorGUILayout.EndHorizontal();
    }
    /*
    void RemoveAllCircuitScripts()
    {
        foreach (GameObject tmpParentObject in Selection.gameObjects)
        {
            foreach (Transform tmpChild in tmpParentObject.GetComponentsInChildren<Transform>())
            {
               
                    if (tmpParentObject.gameObject.GetComponent<CircuitNode>() != null)
                    {
                        tmpParentObject.Destroy(tmpParentObject.gameObject.GetComponent<CircuitNode>());
                    }
                else if (tmpChild.name.Contains("Port"))
                {
                    if (tmpChild.gameObject.GetComponent<CircuitElementPort>() == null)
                    {
                        tmpChild.gameObject.AddComponent<CircuitElementPort>();
                    }
                }
            }
            if (tmpParentObject.gameObject.GetComponent<CircuitBoard>() == null)
            {
                tmpParentObject.gameObject.AddComponent<CircuitBoard>();
            }
        }
    }
    */
    void AddScripts(){
        foreach(GameObject tmpParentObject in Selection.gameObjects){
            foreach(Transform tmpChild in tmpParentObject.GetComponentsInChildren<Transform>())
            {
                if (tmpChild.name.Contains("Node"))
                {
                    if (tmpChild.gameObject.GetComponent<CircuitNode>() == null)
                    {
                        tmpChild.gameObject.AddComponent<CircuitNode>();
                    }
                }
                else if (tmpChild.name.Contains("Port"))
                {
                    if (tmpChild.gameObject.GetComponent<CircuitElementPort>() == null)
                    {
                        tmpChild.gameObject.AddComponent<CircuitElementPort>();
                    }
                    tmpChild.GetComponentInChildren<BoxCollider>().isTrigger = true;
                }
                else if (tmpChild.name.Contains("PositiveRail"))
                {
                    if (tmpChild.gameObject.GetComponent<CircuitNode>() == null)
                    {
                        tmpChild.gameObject.AddComponent<CircuitNode>();
                    }
                }
                else if (tmpChild.name.Contains("NegativeRail"))
                {
                    if (tmpChild.gameObject.GetComponent<CircuitNode>() == null)
                    {
                        tmpChild.gameObject.AddComponent<CircuitNode>();
                    }
                }
            }
            if (tmpParentObject.gameObject.GetComponent<CircuitBoard>() == null)
            {
                tmpParentObject.gameObject.AddComponent<CircuitBoard>();
            }
        }
    }

    void FillScripts()
    {
        foreach (GameObject tmpParentObject in Selection.gameObjects)
        {
            foreach (Transform tmpChild in tmpParentObject.GetComponentsInChildren<Transform>())
            {
                if (tmpChild.name.Contains("Node"))
                {
                        CircuitNode NodeScript = tmpChild.gameObject.GetComponent<CircuitNode>();
                    if (NodeScript != null)
                    {
                        NodeScript.NodeElementPorts = new CircuitElementPort[9];
                        foreach (CircuitElementPort tmp_Port in NodeScript.GetComponentsInChildren<CircuitElementPort>())
                        {
                            NodeScript.NodeElementPorts[int.Parse(tmp_Port.name.Split('_')[1].Trim()) - 1] = tmp_Port;
                        }
                    }

                }
                else if (tmpChild.name.Contains("Port"))
                {
                    CircuitElementPort ElementPortScript = tmpChild.gameObject.GetComponent<CircuitElementPort>();
                    if (ElementPortScript != null)
                    {
                        ElementPortScript.PortNumber = int.Parse(ElementPortScript.name.Split('_')[1].Trim());
                    }
                }
                else if (tmpChild.name.Contains("PositiveRail"))
                {
                    CircuitNode NodeScript = tmpChild.gameObject.GetComponent<CircuitNode>();
                    if (NodeScript != null)
                    {
                        NodeScript.NodeElementPorts = new CircuitElementPort[11];
                        foreach (CircuitElementPort tmp_Port in NodeScript.GetComponentsInChildren<CircuitElementPort>())
                        {
                            NodeScript.NodeElementPorts[int.Parse(tmp_Port.name.Split('_')[1].Trim()) - 1] = tmp_Port;
                        }
                    }
                }
                else if (tmpChild.name.Contains("NegativeRail"))
                {
                    CircuitNode NodeScript = tmpChild.gameObject.GetComponent<CircuitNode>();
                    if (NodeScript != null)
                    {
                        NodeScript.NodeElementPorts = new CircuitElementPort[11];
                        foreach (CircuitElementPort tmp_Port in NodeScript.GetComponentsInChildren<CircuitElementPort>())
                        {
                            NodeScript.NodeElementPorts[int.Parse(tmp_Port.name.Split('_')[1].Trim()) - 1] = tmp_Port;
                        }
                    }
                }
            }

            CircuitBoard CircuitBoardScript = tmpParentObject.gameObject.GetComponent<CircuitBoard>();
            if (CircuitBoardScript != null)
            {
                // Alter values
            }
        }
    }

#endif
}
