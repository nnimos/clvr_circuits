﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircuitElement : MonoBehaviour
{
    public CircuitElementEnds[] ElementEnds; // 0 = Positive, 1 = Negative
    public int ElementType;
    public int ElementValue;

    public void Start()
    {

    }

    public void OnObjectRelease()
    {
        if ((ElementEnds[0].IsTouchingPort && ElementEnds[1].IsTouchingPort) && (ElementEnds[0].CollidingPort != ElementEnds[1].CollidingPort) && (ElementEnds[0].CollidingPort && ElementEnds[1].CollidingPort))
        {

            // Snap the object into place
            Vector3 SnapPosition = Vector3.Lerp(ElementEnds[0].CollidingPort.transform.position, ElementEnds[1].CollidingPort.transform.position, 0.5f);
            SnapPosition.y = this.transform.position.y;
            this.transform.position = SnapPosition;
            ElementEnds[0].CollidingPort.ParentNode.CircuitBoardScript.AddedElements.Add(this);

            if(!ElementEnds[0].CollidingPort.ParentNode.IsNodeActive){
                ElementEnds[0].CollidingPort.ParentNode.CircuitBoardScript.ActivateNode(ElementEnds[0].CollidingPort.ParentNode);
            }

            if(!ElementEnds[1].CollidingPort.ParentNode.IsNodeActive){
                ElementEnds[1].CollidingPort.ParentNode.CircuitBoardScript.ActivateNode(ElementEnds[1].CollidingPort.ParentNode);
            }
        }
        else
        {
            // Delete object
            Object.Destroy(this.gameObject);
        }
    }

    public void WireJoin(){

    }
}
