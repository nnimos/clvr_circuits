﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircuitManager : MonoBehaviour {

    public static Dictionary<int, string> ElementDictionary; // Dictionary of elements. Element name is in netlist format

    private void Start()
    {
        ElementDictionary = new Dictionary<int, string>();
        FillElementDictionary();
    }

    void FillElementDictionary()
    {
        // Element types
        ElementDictionary.Add(-1, ""); // -1 = Unassigned
        ElementDictionary.Add(0, ""); // 0 = Ground
        ElementDictionary.Add(1, "R"); // 1 = Resistor
        ElementDictionary.Add(2, "V"); // 2 = Independent Voltage Source
        ElementDictionary.Add(3, "I"); // 3 = Independent Current Source
        ElementDictionary.Add(4, "C"); // 4 = Capacitor
        ElementDictionary.Add(5, ""); // 5 = Wire (Wire makes the connected nodes equal)
    }
}
