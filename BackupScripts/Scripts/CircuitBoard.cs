﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircuitBoard : MonoBehaviour {

    // Element properties
    public List<CircuitNode> NodeArray;
    public NetlistCreator netlistscript;
    public List<CircuitElement> AddedElements;

    public void Start()
    {
        NodeArray = new List<CircuitNode>();
        AddedElements = new List<CircuitElement>();
    }

    public void ActivateNode(CircuitNode NewNode){
        if(!NodeArray.Contains(NewNode)){
            NodeArray.Add(NewNode);
        }
    }

    public void MergeNodes(CircuitNode Node1, CircuitNode Node2){
        if(NodeArray.Contains(Node2)){
            NodeArray.Remove(Node2);
        }
        Node1.IsConnectedToAnotherNode = true;
        Node2.IsConnectedToAnotherNode = true;
        Node1.ConnectedNode = Node2;
        Node2.ConnectedNode = Node1;
    }

    public void SplitNodes(CircuitNode Node1, CircuitNode Node2){
        if(NodeArray.Contains(Node1)){
            // Split from node 1
            Node1.IsConnectedToAnotherNode = false;
            Node2.IsConnectedToAnotherNode = false;
            Node1.ConnectedNode = null;
            Node2.ConnectedNode = null;

            NodeArray.Add(Node2);
        }
        else
        {
            // Split from node 2
            // Split from node 1
            Node1.IsConnectedToAnotherNode = false;
            Node2.IsConnectedToAnotherNode = false;
            Node1.ConnectedNode = null;
            Node2.ConnectedNode = null;
            NodeArray.Add(Node1);
        }
    }

    // Updates the node voltages. Function is run when the http request is finished
    public void UpdateNodeVoltages(string output)
    {
        string[] NodeVoltages = output.Split('\n');
        foreach (string tmp_String in NodeVoltages)
        {
            if (tmp_String.Contains("="))
            {
                float Voltage = float.Parse(tmp_String.Split('=')[1].Trim().Split('e')[0]);
                int NodeNumber = int.Parse(tmp_String.Split('=')[0].Trim()[1].ToString());
                //NodeArray[NodeNumber].Voltage = Voltage;
            }
        }
    }


}
