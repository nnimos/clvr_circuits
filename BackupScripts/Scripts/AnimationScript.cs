﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationScript : MonoBehaviour {
    // Use this for initialization

    public GameObject FloatingRobot;
    public AudioSource RobotAudioSource;
    public Coroutine OscillateCoroutine;

    public Image FadeImage;

    public AudioClip Audio1;
    public AudioClip Audio2;
    public AudioClip Audio3;
    public AudioClip Audio4;

    // Robot positions
    Vector3 NextToShip = new Vector3(-11.66f, 5.49f, 20.37f);
    Vector3 StartingLocation = new Vector3(-1.7f, 3.23f, 36.99f);

    public bool GoingUp;
    public void Start()
    {
        //to start at zero
        StartCoroutine(Tutorial());
    }

    private IEnumerator Tutorial()
    {
        // Hovering robot
        OscillateCoroutine = StartCoroutine(Oscillate(0.001f));
        yield return new WaitForSeconds(5);
        RobotAudioSource.PlayOneShot(Audio1);
        yield return new WaitForSeconds(8f);
        StartCoroutine(FadeIn(2f));
        yield return new WaitForSeconds(3f);
        RobotAudioSource.PlayOneShot(Audio2);
        yield return new WaitForSeconds(4f);
        StartCoroutine(MoveRobotTo(NextToShip, 2.0f));
        RobotAudioSource.PlayOneShot(Audio3);
        yield return new WaitForSeconds(15f);
        StartCoroutine(MoveRobotTo(StartingLocation, 2.0f));
        RobotAudioSource.PlayOneShot(Audio4);
        yield return new WaitForSeconds(18f);

    }

    private IEnumerator FadeIn(float duration)
    {
        float elapsedTime = 0.0f;
        Color OriginalColor = FadeImage.color;
        Color NewColor = FadeImage.color;
        NewColor.a = 0f;
        while (elapsedTime < duration)
        {
            elapsedTime += Time.deltaTime;
            FadeImage.color = Color.Lerp(OriginalColor, NewColor, elapsedTime / duration);
            yield return null;
        }
    }

    private IEnumerator Oscillate(float BounceValue)
    {
        while (true)
        {
            // Oscillate
                FloatingRobot.transform.position += Vector3.up * Mathf.Sin(Time.time) * BounceValue;
            yield return null;
        }
    }

    private IEnumerator MoveRobotTo(Vector3 NewPosition, float duration)
    {
        float elapsedTime = 0.0f;
        Vector3 OriginalPosition = FloatingRobot.transform.position;
        while (elapsedTime < duration)
        {
            elapsedTime += Time.deltaTime;
            FloatingRobot.transform.position = Vector3.Slerp(OriginalPosition, NewPosition, elapsedTime / duration);
            yield return null;
        }
    }
}

