﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircuitSim : MonoBehaviour {

	public static Dictionary<int, string> ElementDictionary; // Dictionary of elements. Element name is in netlist format
	public static Dictionary<int, Color32> ElementColorDictionary;
	public static List<Element> ElementList; // List of elements
	public static List<Node> NodeList; // List of nodes
	public static int NodeIncrementer; // Increments the node numbers
	public static int ElementIncrementer;
	public static Node[,] NodeMap; // Map of the nodes in the MxN grid
	public static Node[] NodeArray; // Array of the nodes
	public static int M; // Width of the grid
	public static int N; // Height of the grid
	public static Vector3 CentrePoint; // Centre of the grid
	public static float BoxColliderSize; // Size of the box collider
	public static float NodeSpacing;
	public NetlistCreator netlistscript;
	public static LineRenderer CircuitLineRenderer;
	public static Material NodeMaterial;
	public static Material LineRendererMaterial;
	private void Start()
	{
		netlistscript = GetComponent<NetlistCreator>();
		Initialise();
		//TestCircuit();
		//netlistscript.CreateNetlist();
	}

	// Initialise the variables
	void Initialise(){
		CircuitLineRenderer = this.gameObject.GetComponent<LineRenderer>();
		NodeMaterial = Resources.Load<Material>("Materials/ElementMaterial");
		LineRendererMaterial = Resources.Load<Material>("Materials/LineRendererMaterial");
		ChosenNodes = new List<Node>();
		ElementList = new List<Element>(); // Initialise the Element List
		NodeList = new List<Node>(); // Initialise the Node list
		ElementDictionary = new Dictionary<int, string>();
		ElementColorDictionary = new Dictionary<int, Color32>();
		FillElementDictionary(); //
		NodeIncrementer = 0; // Initialise the Node Incrementer
		ElementIncrementer = 1;
		M = 5; // Initialise the width of the grid
		N = 5; // Initialise the height of the grid
		NodeMap = new Node[M, N]; // Initialise the grid size
		NodeArray = new Node[M * N];
		CentrePoint = new Vector3(0f, 0f, 0f); // Initialise the centre of the grid
		BoxColliderSize = 1f; // Initialise the size of the box collider
		NodeSpacing = 10f;

		CreateNodeGrid();
	}

	void FillElementDictionary(){

		// Element types
		ElementDictionary.Add(-1, ""); // -1 = Unassigned
		ElementColorDictionary.Add(-1, new Color32(255, 255, 255, 255));
		ElementDictionary.Add(0, ""); // 0 = Ground
		ElementColorDictionary.Add(0, new Color32(0, 255, 255, 255));
		ElementDictionary.Add(1, "R"); // 1 = Resistor
		ElementColorDictionary.Add(1, new Color32(255, 0, 255, 255));
		ElementDictionary.Add(2, "V"); // 2 = Independent Voltage Source
		ElementColorDictionary.Add(2, new Color32(255, 255, 0, 255));
		ElementDictionary.Add(3, "I"); // 3 = Independent Current Source
		ElementColorDictionary.Add(3, new Color32(128, 255, 255, 255));
		ElementDictionary.Add(4, "C"); // 4 = Capacitor
		ElementColorDictionary.Add(4, new Color32(255, 128, 255, 255));
		ElementDictionary.Add(5, ""); // 5 = Wire (Wire makes the connected nodes equal)
		ElementColorDictionary.Add(5, new Color32(255, 255, 255, 255));
	}

    // Create a grid of nodes
	void CreateNodeGrid(){
		
		for (int i = -(M / 2); i <= M / 2; i++) // Go along the width
        {
            for (int j = -(N / 2); j <= N / 2; j++) // Go along the height
            {
                // Create element container and add required objects
				GameObject NodeGameObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				NodeGameObject.name = "Element_(" + i + "," + j + ")";
				Node tmp_Container = NodeGameObject.AddComponent<Node>();
				tmp_Container.NodeMeshRenderer = tmp_Container.GetComponent<MeshRenderer>(); // Get the elements mesh renderer
				tmp_Container.NodeMeshRenderer.material = NodeMaterial;
				tmp_Container.OriginalColor = tmp_Container.NodeMeshRenderer.material.color; // Get the elements original colour

					
                // Scale the node to the correct size
				NodeGameObject.transform.position = CentrePoint + new Vector3(i * NodeSpacing, 0, j * NodeSpacing);
				tmp_Container.CentreCoordinate = NodeGameObject.transform.position;
				NodeGameObject.transform.localScale = Vector3.one * BoxColliderSize;
				BoxCollider tmp_BoxCollider = NodeGameObject.AddComponent<BoxCollider>();
				tmp_BoxCollider.size = new Vector3(NodeSpacing / 2, NodeSpacing, NodeSpacing / 2);
				NodeGameObject.layer = 8;

                
                NodeMap[M / 2 + i, N / 2 + j] = tmp_Container;

            }
        }
	}

	void TestCircuit(){
		// Add ground
		GameObject GroundObject = new GameObject();
		Element Ground = GroundObject.AddComponent<Element>();
		Ground.InitialiseElement(NodeMap[0, 0], NodeMap[0, 0], 0, 0);
		if (!CircuitSim.ElementList.Contains(Ground))
        {
			CircuitSim.ElementList.Add(Ground);
        }

        // Add Independent voltage source 1
		GameObject IndVoltageSource1Object = new GameObject();
		Element IndVoltageSource1 = IndVoltageSource1Object.AddComponent<Element>();
		IndVoltageSource1.InitialiseElement(NodeMap[0, 1], NodeMap[0, 0], 2, 25);
		if (!CircuitSim.ElementList.Contains(IndVoltageSource1))
        {
			CircuitSim.ElementList.Add(IndVoltageSource1);
        }

		// Add Independent voltage source 1
		GameObject IndVoltageSource2Object = new GameObject();
		Element IndVoltageSource2 = IndVoltageSource2Object.AddComponent<Element>();
		IndVoltageSource2.InitialiseElement(NodeMap[1, 0], NodeMap[0, 0], 2, 15);
		if (!CircuitSim.ElementList.Contains(IndVoltageSource2))
        {
			CircuitSim.ElementList.Add(IndVoltageSource2);
        }

		// Add Resistor 1
		GameObject Resistor1Object = new GameObject();
		Element Resistor1 = Resistor1Object.AddComponent<Element>();
		Resistor1.InitialiseElement(NodeMap[0, 1], NodeMap[1, 1], 1, 20);
		if (!CircuitSim.ElementList.Contains(Resistor1))
        {
			CircuitSim.ElementList.Add(Resistor1);
        }

		// Add Resistor 2
		GameObject Resistor2Object = new GameObject();
		Element Resistor2 = Resistor2Object.AddComponent<Element>();
		Resistor2.InitialiseElement(NodeMap[1, 1], NodeMap[1, 0], 1, 10);
		if (!CircuitSim.ElementList.Contains(Resistor2))
        {
			CircuitSim.ElementList.Add(Resistor2);
        }

		// Add Resistor 3
		GameObject Resistor3Object = new GameObject();
		Element Resistor3 = Resistor3Object.AddComponent<Element>();
		Resistor3.InitialiseElement(NodeMap[1, 1], NodeMap[0, 0], 1, 4.7f);
		if (!CircuitSim.ElementList.Contains(Resistor3))
        {
			CircuitSim.ElementList.Add(Resistor3);
        }
	}

    // Updates the node voltages. Function is run when the http request is finished
	public static void UpdateNodeVoltages(string output){
		string[] NodeVoltages = output.Split('\n');
		foreach(string tmp_String in NodeVoltages){
			if(tmp_String.Contains("=")){
				float Voltage = float.Parse(tmp_String.Split('=')[1].Trim().Split('e')[0]);
				int NodeNumber = int.Parse(tmp_String.Split('=')[0].Trim()[1].ToString());
				NodeArray[NodeNumber].Voltage = Voltage;
			}         
		}
	}

	// Add components
	public static bool IsChoosingNode = false;
	public static List<Node> ChosenNodes;
	public static float ChosenValue;
	public static int ChosenElement;
    

	public static void AddNewElement(){
		GameObject NewObject = new GameObject();
		Element NewElement = NewObject.AddComponent<Element>();
		NewElement.InitialiseElement(ChosenNodes[0], ChosenNodes[1], ChosenElement, ChosenValue);
		if (!CircuitSim.ElementList.Contains(NewElement))
        {
			CircuitSim.ElementList.Add(NewElement);
        }
		NewElement.DrawLine(ChosenNodes[0].CentreCoordinate, ChosenNodes[1].CentreCoordinate);

		foreach(Node tmp_Node in ChosenNodes){
			tmp_Node.ResetColour();
		}

		IsChoosingNode = false;
		ChosenNodes.Clear();
	}

	public void RemoveElement(){
		
	}

	static void ConnectElement(){
		
	}



	public void ActicateNewElement(){
		IsChoosingNode = true;
	}

	public void AddResistor(){
		ChosenValue = 10f;
		ChosenElement = 1;
		ActicateNewElement();
	}

	public void AddGround(){
		ChosenValue = 0f;
        ChosenElement = 0;
		ActicateNewElement();
	}
    
	public void AddIndepVoltageSrc(){
		ChosenValue = 2f;
        ChosenElement = 2;
		ActicateNewElement();
	}

	public void AddIndepCurrentSrc()
    {
		ChosenValue = 2f;
        ChosenElement = 3;
		ActicateNewElement();
    }

	public void AddWire(){
		ChosenValue = 0f;
        ChosenElement = 5;
        ActicateNewElement();
	}
}
