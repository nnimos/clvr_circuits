﻿// This script is the main script that is attached to the player. 
// It controls everything the player does and how it interacts with the server and other world objects

using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using DaydreamElements.ClickMenu;
using DaydreamElements.ObjectManipulation;
using Gvr.Internal;
using UnityEngine.UI;
using GoogleVR.KeyboardDemo;

/// The Painter manages a collection of paint strokes and is responsible
/// for creating, modifying, and removing the strokes.
public class PlayerManager : NetworkBehaviour
{
    public GameObject AddedObject;
    public CircuitElement ElementScript;
    public bool IsAddingElement = false;
    public CircuitElement WireElement;
    public GvrAndManipulationPointer PointerScript;
    public GvrLaserVisual LaserVisualScript;
    public LayerMask WireLayerMask;
    public GameObject ChildObjects;
    public GameObject VoltagePrefab;
    public GameObject VoltageObject;
    public TextMesh VoltageTextMesh;
    public TextMesh VRCurrentFunctionUIText;
    public GameObject KeyboardObjects;
    public KeyboardDelegateCLVR DelegateScript;
    public bool IsRemovingElement = false;

    public GameObject CharacterModel;

    // Values set after menu button pressed
    public int ElementToAdd;
    public Vector3 ElementPosition;

    public float ElementValue = 0f;

    [SyncVar]
    public int ElementIdentifierCount = 0;

    Vector2 mousePos = new Vector2();

    // Wire variables
    public bool IsAddingWire = false;
    public bool FirstClick = false;
    public bool HasRunStartFunctions = false;

    public LineRenderer WireLineRenderer;



    #region KeyboardItems
    // Keyboard variables
    public bool IsUsingKeyboard = false;
    public string CurrentKeyboardText;

    // Hides the keyboard from the screen
    public void DeactivateKeyboard()
    {
        IsUsingKeyboard = false;
        KeyboardObjects.SetActive(false);
    }

    // Shows the keyboard on the screen
    public void ActivateKeyboard()
    {
        IsUsingKeyboard = true;
        KeyboardObjects.SetActive(true);
    }

    // Get the text input on the keyboard
    public void UpdateKeyboardTextInput(string KeyboardInput)
    {
        VRCurrentFunctionUIText.text = "Element value: " + KeyboardInput;
        CurrentKeyboardText = KeyboardInput;
    }

    // This function is only used in the editor when the VR keyboard isnt available
    public void KeyboardPressedEnter(string KeyboardInput)
    {
        if (!float.TryParse(KeyboardInput, out ElementValue))
        {
            RemoveCurrentAddedObject();
            KeyboardObjects.SetActive(false);
            return;
        }
        CmdAddElementObject(ElementPosition, ElementValue, ElementToAdd);
        KeyboardObjects.SetActive(false);
        IsAddingElement = true;
    }
    #endregion

    #region VoltmeterItems

    // Voltmeter variables
    public bool IsVoltmeterActivated = false;
    public float CurrentVoltage = 0f;

    // Turns off the voltmeter
    public void DeactivateVoltmeter()
    {
        IsVoltmeterActivated = false;
        VoltageObject.SetActive(false);
    }

    // Activates the voltmeter
    public void ActivateVoltmeter()
    {
        IsVoltmeterActivated = true;
        VoltageObject.SetActive(true);
    }

    #endregion

    // Removes the current added element
    public void RemoveCurrentAddedObject()
    {
        CmdDestroyObject(AddedObject);
        IsAddingWire = false;
        IsAddingElement = false;
    }

    #region ServerCommands

    // Request the server to destroy an object
    [Command]
    void CmdDestroyObject(GameObject ObjectToDestroy)
    {
        NetworkServer.Destroy(ObjectToDestroy);
    }

    // Request the server to give the player authority on an object
    [Command]
    void CmdSetAuthority(NetworkIdentity grabID, NetworkIdentity playerID)
    {
        grabID.AssignClientAuthority(playerID.connectionToClient);
    }

    // Request the server to remove the player authority on an object
    [Command]
    void CmdRemoveAuthority(NetworkIdentity grabID, NetworkIdentity playerID)
    {
        grabID.RemoveClientAuthority(playerID.connectionToClient);
    }

    // Request the server to simulate the currently built circuit
    [Command]
    void CmdCreateNetlistAndSimulate()
    {
        NetlistCreator.CreateNetlist(TestScript.InstantiatedObject.GetComponent<CircuitBoard>());
    }

    // Request the server to add a circuit element
    [Command]
    public void CmdAddElementObject(Vector3 ElementPosition, float ElementValue, int ElementType)
    {
        AddedObject = Instantiate(CircuitManager.ElementDictionary[ElementType].ElementPrefab);
        AddedObject.transform.position = ElementPosition;
        AddedObject.GetComponent<CircuitElement>().ElementValue = ElementValue;
        AddedObject.GetComponent<CircuitElement>().parentNetId = this.netId;
        NetworkServer.Spawn(AddedObject);
        CmdSetAuthority(AddedObject.GetComponent<NetworkIdentity>(), this.GetComponent<NetworkIdentity>());
    }

    // Request the server to add a wire for all other players when it has been placed properly
    [Command]
    public void CmdAddCompletedWire(Vector3 LinePosition1, Vector3 LinePosition2, int PostiveNodeNumber, int NegativeNodeNumber)
    {
        //RemoveCurrentAddedObject();
        GameObject TempObject = Instantiate(CircuitManager.ElementDictionary[5].ElementPrefab);
        CircuitElement tmp_CircuitElementScript = TempObject.GetComponent<CircuitElement>();
        tmp_CircuitElementScript.parentNetId = this.netId;
        tmp_CircuitElementScript.PositiveNode = PostiveNodeNumber;
        tmp_CircuitElementScript.NegativeNode = NegativeNodeNumber;
        tmp_CircuitElementScript.LinePosition1 = LinePosition1;
        tmp_CircuitElementScript.LinePosition2 = LinePosition2;
        NetworkServer.Spawn(TempObject);
        CmdSetAuthority(TempObject.GetComponent<NetworkIdentity>(), this.GetComponent<NetworkIdentity>());
    }

    #endregion

    public void LocalyCreateWire()
    {
        AddedObject = Instantiate(CircuitManager.ElementDictionary[5].ElementPrefab);
        ElementScript = AddedObject.GetComponent<CircuitElement>();
        WireLineRenderer = AddedObject.GetComponent<LineRenderer>();
        WireLineRenderer.enabled = false;
    }

    void Awake()
    {
        if (isLocalPlayer)
        {
            ChildObjects.SetActive(true);
            CharacterModel.SetActive(false);
            VoltageObject = Instantiate(VoltagePrefab);
            VoltageTextMesh = VoltageObject.GetComponent<TextMesh>();
            VoltageTextMesh.gameObject.SetActive(false);
            CurrentPlayer.LocalPlayer = this;
            menuRoot.OnItemSelected += OnItemSelected;
            HasRunStartFunctions = true;
        }
    }

    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        else if (!HasRunStartFunctions)
        {
            Awake();
        }
        if (WireElement)
        {
            WireElement.CmdAddElementWire();
            WireElement = null;
        }

#if UNITY_EDITOR
        if (Input.GetKeyUp(KeyCode.Return))
        {
            KeyboardPressedEnter(CurrentKeyboardText);
        }
#endif
        if (IsAddingElement && AddedObject)
        {

            if (GvrControllerInput.ClickButtonDown)
            {
                // Stop adding element
                OnObjectRelease();
                ElementScript.ElementIdentifier = ElementIdentifierCount;
                ElementIdentifierCount += 1;
                AddedObject = null;
                ElementScript = null;
                IsAddingElement = false;
                return;
            }

        }
        else if (IsAddingWire && WireLineRenderer && AddedObject)
        {
            if (GvrControllerInput.ClickButtonDown)
            {
                if (!FirstClick)
                {
                    if (PointerScript.CheckForPort())
                    {
                        FirstClick = true;
                        ElementScript.PositiveNode = PointerScript.PortRaycastResult.transform.gameObject.GetComponent<CircuitElementPort>().NodeNumber;
                        WireLineRenderer.enabled = true;
                        WireLineRenderer.SetPosition(0, PointerScript.PortRaycastResult.transform.position);
                        return;
                    }
                }
                else
                {
                    // Second node
                    if (ElementScript.NegativeNode != 0)
                    {
                        ElementScript.ElementIdentifier = ElementIdentifierCount;
                        ElementIdentifierCount += 1;
                        CmdAddCompletedWire(WireLineRenderer.GetPosition(0), WireLineRenderer.GetPosition(1), ElementScript.PositiveNode, ElementScript.NegativeNode);
                    }
                    Destroy(AddedObject);
                    AddedObject = null;
                    FirstClick = false;
                    IsAddingWire = false;
                    return;
                }
            }
            else if (FirstClick)
            {
                if (PointerScript.CheckForPort())
                {
                    ElementScript.NegativeNode = PointerScript.PortRaycastResult.transform.gameObject.GetComponent<CircuitElementPort>().NodeNumber;
                    WireLineRenderer.SetPosition(1, PointerScript.PortRaycastResult.transform.position);

                }
            }
        }
        else if (IsVoltmeterActivated)
        {
            if (PointerScript.CheckForPort())
            {
                CurrentVoltage = PointerScript.PortRaycastResult.transform.GetComponent<CircuitElementPort>().ParentNode.Voltage;
                VoltageTextMesh.text = CurrentVoltage + "V";
                VoltageObject.transform.position = PointerScript.PortRaycastResult.transform.position + new Vector3(0, 0.2f, 0);
                VoltageObject.transform.LookAt(Camera.main.transform);
            }
        }
        else if (IsRemovingElement)
        {
            if (GvrControllerInput.ClickButtonDown)
            {
                if (PointerScript.CheckForElement())
                {
                    CmdSetAuthority(PointerScript.PortRaycastResult.transform.gameObject.GetComponent<NetworkIdentity>(), this.GetComponent<NetworkIdentity>());
                    CircuitElement CircuitElementScript = PointerScript.PortRaycastResult.transform.gameObject.GetComponent<CircuitElement>();
                    CircuitElementScript.CmdRemoveElement();
                    CmdDestroyObject(CircuitElementScript.gameObject);
                }
            }
        }
    }

    #region MenuItems
    // The root menu object
    public ClickMenuRoot menuRoot;

    // Close all current functions when the menu is opened
    public void CloseAllOnMenuOpen()
    {
        if (IsAddingWire || IsAddingElement)
        {
            CmdDestroyObject(AddedObject);
            IsAddingWire = false;
            IsAddingElement = false;
        }
        else if (IsVoltmeterActivated)
        {
            DeactivateVoltmeter();
        }
        else if (IsRemovingElement)
        {
            IsRemovingElement = false;
        }
        VRCurrentFunctionUIText.text = "";
    }

    // Function is executed when a menu item is clicked
    private void OnItemSelected(ClickMenuItem item)
    {
        if (!isLocalPlayer)
        {
            return;
        }
        int id = (item ? item.id : -1);
        switch (id)
        {
            case 1:
                // Resistor
                if (!PointerScript.CheckForBoard(LaserVisualScript.transform.TransformPoint(LaserVisualScript.Laser.GetPosition(1))))
                {
                    Debug.Log(LaserVisualScript.transform.TransformPoint(LaserVisualScript.Laser.GetPosition(1)));
                    VRCurrentFunctionUIText.text = "No board found. Please select on top of board.";
                    break;
                }
                if (IsAddingWire || IsAddingElement)
                {
                    RemoveCurrentAddedObject();
                }
                ElementToAdd = 1;
                ElementPosition = PointerScript.BoardRaycastResult.point;
                ElementPosition.y = 0.5413f;
                ActivateKeyboard();
                VRCurrentFunctionUIText.text = "Please type the element value";
                break;
            case 2:
                // Wire
                if (IsAddingWire || IsAddingElement)
                {
                    RemoveCurrentAddedObject();
                }
                LocalyCreateWire();
                IsAddingWire = true;
                VRCurrentFunctionUIText.text = "Adding wire";
                break;
            case 3:
                // Vdc
                if (!PointerScript.CheckForBoard(LaserVisualScript.transform.TransformPoint(LaserVisualScript.Laser.GetPosition(1))))
                {
                    Debug.Log(LaserVisualScript.transform.TransformPoint(LaserVisualScript.Laser.GetPosition(1)));
                    VRCurrentFunctionUIText.text = "No board found. Please select on top of board.";
                    break;
                }
                if (IsAddingWire || IsAddingElement)
                {
                    RemoveCurrentAddedObject();
                }
                ElementToAdd = 2;
                ElementPosition = PointerScript.BoardRaycastResult.point;
                ElementPosition.y = 0.5414f;
                ActivateKeyboard();
                VRCurrentFunctionUIText.text = "Please type the element value";
                break;
            case 4:
                // Ground
                if (!PointerScript.CheckForBoard(LaserVisualScript.transform.TransformPoint(LaserVisualScript.Laser.GetPosition(1))))
                {
                    Debug.Log(LaserVisualScript.transform.TransformPoint(LaserVisualScript.Laser.GetPosition(1)));
                    VRCurrentFunctionUIText.text = "No board found. Please select on top of board.";
                    break;
                }
                if (IsAddingWire || IsAddingElement)
                {
                    RemoveCurrentAddedObject();
                }
                ElementToAdd = 0;
                ElementValue = 0;
                ElementPosition = PointerScript.BoardRaycastResult.point;
                ElementPosition.y = 0.645f;
                VRCurrentFunctionUIText.text = "Please type the element value";
                CmdAddElementObject(ElementPosition, ElementValue, ElementToAdd);
                IsAddingElement = true;
                break;
            case 6:
                // Leave server
                if (isServer)
                {
                    NetworkManager.singleton.StopHost();
                }
                else
                {
                    NetworkManager.singleton.StopClient();
                }

                break;
            case 7:
                // Simulate netlist
                CmdCreateNetlistAndSimulate();
                break;
            case 8:
                // Voltmeter
                ActivateVoltmeter();
                VRCurrentFunctionUIText.text = "Using voltmeter";
                break;
            case 12:
                // Remove component
                VRCurrentFunctionUIText.text = "Removing element";
                IsRemovingElement = true;
                break;
        }
    }

    #endregion

    void OnDestory()
    {
        //Clear();
    }

    // Executed when an element is released
    public void OnObjectRelease()
    {
        if (ElementScript.ElementType != 0)
        {
            if ((ElementScript.ElementEnds[0].IsTouchingPort && ElementScript.ElementEnds[1].IsTouchingPort) && (ElementScript.ElementEnds[0].CollidingPort.NodeNumber != ElementScript.ElementEnds[1].CollidingPort.NodeNumber) && (ElementScript.ElementEnds[0].CollidingPort && ElementScript.ElementEnds[1].CollidingPort))
            {
                // Snap the object into place
                Vector3 SnapPosition = Vector3.Lerp(ElementScript.ElementEnds[0].CollidingPort.transform.position, ElementScript.ElementEnds[1].CollidingPort.transform.position, 0.5f);
                Vector3 SnapDirection = (ElementScript.ElementEnds[0].CollidingPort.transform.position - ElementScript.ElementEnds[1].CollidingPort.transform.position).normalized;
                ElementScript.IsSet = true;
                SnapPosition.y = AddedObject.transform.position.y;
                AddedObject.transform.position = SnapPosition;
                AddedObject.transform.rotation = Quaternion.LookRotation(SnapDirection, SnapPosition);
                AddedObject.transform.eulerAngles = new Vector3(SnapDirection.x, SnapDirection.y, 0);
                ElementScript.ObjectRenderer.material.color = ElementScript.OriginalColor;
                ElementScript.CmdAddElement(ElementScript.ElementEnds[0].CollidingPort.NodeNumber, ElementScript.ElementEnds[1].CollidingPort.NodeNumber);
            }
            else
            {
                // Delete object
                CmdDestroyObject(AddedObject.gameObject);
            }
        }
        else
        {
            if (ElementScript.ElementEnds[0].CollidingPort)
            {
                // Snap the object into place
                Vector3 SnapPosition = ElementScript.ElementEnds[0].CollidingPort.transform.position;
                ElementScript.IsSet = true;
                SnapPosition.y = AddedObject.transform.position.y;
                AddedObject.transform.position = SnapPosition;
                ElementScript.ObjectRenderer.material.color = ElementScript.OriginalColor;
                ElementScript.CmdAddElementGround(ElementScript.ElementEnds[0].CollidingPort.NodeNumber);
            }
            else
            {
                // Delete object
                CmdDestroyObject(AddedObject.gameObject);
            }
        }
    }

}

