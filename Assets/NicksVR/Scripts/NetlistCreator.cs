﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class NetlistCreator : MonoBehaviour
{
    static string ServerAddress = "http://35.201.25.96/";
    static public NetlistCreator instance;

    void Awake()
    { //called when an instance awakes in the game
        instance = this; //set our static reference to our newly initialized instance
    }

    public static void CreateNetlist(CircuitBoard Board)
    {
        string FinishedString = "", NodePositive_str = "", NodeNegative_str = "";
        foreach (CircuitElement tmp_Element in Board.AddedElements)
        {
            // Check if node positive is grounded
            if (tmp_Element.PositiveNode == Board.GroundedNode)
            {
                NodePositive_str = "gnd";
            }
            else
            {
                NodePositive_str = "n" + tmp_Element.PositiveNode.ToString();
            }

            // Check if node negative is grounded
            if (tmp_Element.NegativeNode == Board.GroundedNode)
            {
                NodeNegative_str = "gnd";
            }
            else
            {
                NodeNegative_str = "n" + tmp_Element.NegativeNode.ToString();
            }

            // Error has occured
            if (tmp_Element.PositiveNode == 0 || tmp_Element.NegativeNode == 0){
                continue;
            }
            if (tmp_Element.ElementType == 0)
            {
                // Ground, and do nothing
                
            }
            else if(tmp_Element.ElementType == 5){
                FinishedString = FinishedString +  string.Format("TLIN:Line{0} {1} {2} Z=\"0.00000000001 Ohm\" L=\"0.00000001 mm\" Alpha=\"0 dB\" Temp=\"26.85\"\r\n", tmp_Element.ElementIdentifier, NodePositive_str, NodeNegative_str);
            }
            else
            {
                // Element
                FinishedString = FinishedString + AddElement(tmp_Element.ElementType, NodePositive_str, NodeNegative_str, tmp_Element.ElementValue, tmp_Element.ElementIdentifier, Board);
            }
        }
        FinishedString = FinishedString + ".DC:DC1 Temp = \"26.85\" reltol = \"0.001\" abstol = \"1 pA\" vntol = \"1 uV\" saveOPs = \"no\" MaxIter = \"150\" saveAll = \"no\" convHelper = \"none\" Solver = \"CroutLU\"";
        //FinishedString += AddOptions(CircuitSim.NodeList);
        SendRequest(FinishedString, Board);
        Debug.Log(FinishedString);
    }

    static string AddElement(int Type, string NodePositive, string NodeNegative, float Value, int ElementNumber, CircuitBoard Board)
    {
        return string.Format("{0} {1} {2} {3}\r\n", CircuitManager.ElementDictionary[Type].ElementString[0] + ":" + CircuitManager.ElementDictionary[Type].ElementString[0] + ElementNumber, NodePositive, NodeNegative, CircuitManager.ElementDictionary[Type].ElementString[1] + Value + " " + CircuitManager.ElementDictionary[Type].ElementString[2] + "\"");


    }

    /*
	public void CreateNetlist()
	{
		string FinishedString = "* Circuit.cir\r\n";
		foreach(Element tmp_Element in CircuitSim.ElementList)
		{
			if(!tmp_Element.IsGround && !tmp_Element.IsWire){
				FinishedString = FinishedString + AddElement(tmp_Element.Type, tmp_Element.NodeList[0], tmp_Element.NodeList[1], tmp_Element.Value, tmp_Element.ElementNumber);
			}
			else if(tmp_Element.IsWire){
				
			}

		}
		FinishedString += AddOptions(CircuitSim.NodeList);
		SendRequest(FinishedString);
		Debug.Log(FinishedString);
	}

	string AddOptions(List<Node> NodeList)
	{
		string Options = ".control\r\n op\r\n option numdgt = 2\r\n set wr_singlescale\r\n set noprintscale\r\n";
		bool IsFirstRun = true;
		foreach(Node tmp_Node in NodeList){
			
			if(!tmp_Node.IsGround){
				if (IsFirstRun)
                {
                    IsFirstRun = false;
					Options += string.Format("print n{0} > /var/www/scripts/circuitresult.txt\r\nset appendwrite\r\n", tmp_Node.NodeNumber);
                }
                else
                {
					Options += string.Format("print n{0} >> /var/www/scripts/circuitresult.txt\r\n", tmp_Node.NodeNumber);
                }
			}         
		}

		Options+= ".endc\r\n.end";

		return Options;
	}

*/

    static void SendRequest(string NetList, CircuitBoard Board)
    {
        instance.StartCoroutine(RequestNodes(NetList, Board));
	}

    static IEnumerator RequestNodes(string NetList, CircuitBoard Board)
    {
        WWWForm form = new WWWForm();
        form.AddField("netlist", NetList);
        UnityWebRequest www = UnityWebRequest.Post(ServerAddress, form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            while (!www.isDone)
            {
                yield return null;
            }

            int n = 4;
            string[] lines = www.downloadHandler.text.Split(Environment.NewLine.ToCharArray()).Skip(n).ToArray();

            string output = string.Join(Environment.NewLine, lines);
            Board.UpdateNodeVoltages(www.downloadHandler.text);
        }
    } 

}
