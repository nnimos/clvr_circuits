﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CircuitManager : MonoBehaviour {

    public class ElementProperties{
        public string[] ElementString;
        public GameObject ElementPrefab;
    }

    public static Dictionary<int, ElementProperties> ElementDictionary; // Dictionary of elements. Element name is in netlist format
    public GameObject ResistorObject;
    public GameObject GroundObject;
    public GameObject IndepVoltageSourceObject;
    public GameObject IndepCurrentSourceObject;
    public GameObject CapacitorObject;
    public GameObject WireObject;
    public Camera tmp_Camera;

    private void Start()
    {
        ElementDictionary = new Dictionary<int, ElementProperties>();
        FillElementDictionary();
    }

    void FillElementDictionary()
    {
        // -1 = Unassigned
        ElementProperties UnassignedProperties = new ElementProperties
        {
            ElementString = new string[] { "R", "R=\"", "Ohm" },
            ElementPrefab = ResistorObject
        };

        ElementDictionary.Add(-1, UnassignedProperties);

        // 0 = Ground
        ElementProperties GroundProperties = new ElementProperties
        {
            ElementString = new string[] { "R", "R=\"", "Ohm" },
            ElementPrefab = GroundObject
        };

        ElementDictionary.Add(0, GroundProperties);

        // 1 = Resistor
        ElementProperties ResistorProperties = new ElementProperties
        {
            ElementString = new string[] { "R", "R=\"", "Ohm" },
            ElementPrefab = ResistorObject
        };

        ElementDictionary.Add(1, ResistorProperties);

        // 2 = Independent Voltage Source
        ElementProperties IndepVoltageSourceProperties = new ElementProperties
        {
            ElementString = new string[] { "Vdc", "U=\"", "V" },
        ElementPrefab = IndepVoltageSourceObject
        };

        ElementDictionary.Add(2, IndepVoltageSourceProperties);


        // 3 = Independent Current Source
        ElementProperties IndepCurrentSourceObjectProperties = new ElementProperties
        {
            ElementString = new string[] { "R", "R=\"", "Ohm" },
            ElementPrefab = IndepCurrentSourceObject
        };

        ElementDictionary.Add(3, IndepCurrentSourceObjectProperties);

        // 4 = Capacitor
        ElementProperties CapacitorProperties = new ElementProperties
        {
            ElementString = new string[] { "R", "R=\"", "Ohm" },
            ElementPrefab = CapacitorObject
        };

        ElementDictionary.Add(4, CapacitorProperties);

        // 5 = Wire (Wire makes the connected nodes equal)
        ElementProperties WireProperties = new ElementProperties
        {
            ElementString = new string[] { "R", "R=\"", "Ohm" },
            ElementPrefab = WireObject
        };

        ElementDictionary.Add(5, WireProperties);
    }
}
