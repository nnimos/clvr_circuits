﻿using System;
using UnityEngine;
using UnityEngine.Networking;

public class TestScript : NetworkBehaviour
{
    public GameObject circuitBoardPrefab;
    public static GameObject InstantiatedObject;


    public override void OnStartServer()
    {
        InstantiatedObject = Instantiate(circuitBoardPrefab);
        NetworkServer.Spawn(InstantiatedObject);
        this.gameObject.AddComponent<NetlistCreator>();
    }

    public static GameObject spawnElement(GameObject tmp_Object){
        GameObject InstantiatedElement = Instantiate(tmp_Object, InstantiatedObject.transform);
        NetworkServer.Spawn(InstantiatedElement);
        return InstantiatedElement;
    }

    public void CreateNetlistAndSimulate(){
        NetlistCreator.CreateNetlist(InstantiatedObject.GetComponent<CircuitBoard>());
    }

}