﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CircuitElementEnds : MonoBehaviour
{
    public bool IsTouchingPort = false;
    public CircuitElementPort CollidingPort;

    void OnTriggerEnter(Collider other)
    {
        IsTouchingPort = true;
        CollidingPort = other.gameObject.GetComponent<CircuitElementPort>();
    }

    private void OnTriggerExit(Collider other)
    {
        if(CollidingPort){
            if (CollidingPort.gameObject == other.gameObject)
            {
                CollidingPort = null;
            }
        }
    }
}
