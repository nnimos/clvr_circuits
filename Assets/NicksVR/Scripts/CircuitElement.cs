﻿using System.Collections;
using System.Collections.Generic;
using DaydreamElements.ObjectManipulation;
using UnityEngine;
using UnityEngine.Networking;

public class CircuitElement : NetworkBehaviour
{
    public CircuitElementEnds[] ElementEnds; // 0 = Positive, 1 = Negative
    public LineRenderer ElementLineRenderer;
    public MoveablePhysicsObject PhysicsObjectScript;
    public Rigidbody CircuitElementRigidBody;
    public GameObject ChildrenObjects;
    public CapsuleCollider WireCapsuleCollider;
    public MeshRenderer ObjectRenderer;
    public Color OriginalColor;
    public bool IsSet = false;
    public bool IsTouchingTwoDifferentPorts = false;

    [SyncVar]
    public int ElementType;

    [SyncVar]
    public float ElementValue;

    [SyncVar]
    public int ElementIdentifier;

    [SyncVar]
    public NetworkInstanceId parentNetId;

    [SyncVar]
    public int PositiveNode = 0;

    [SyncVar]
    public int NegativeNode = 0;

    [SyncVar]
    public Vector3 LinePosition1;

    [SyncVar]
    public Vector3 LinePosition2;

    private void Update()
    {
        if (!IsSet)
        {
            if (ElementType != 0)
            {
                if (ElementEnds[0] && ElementEnds[1])
                {
                    if (ElementEnds[0].CollidingPort && ElementEnds[1].CollidingPort)
                    {
                        if (ElementEnds[0].CollidingPort.NodeNumber != ElementEnds[1].CollidingPort.NodeNumber)
                        {
                            // Touching two different ports
                            if (!IsTouchingTwoDifferentPorts)
                            {
                                ObjectRenderer.material.color = Color.red;
                                IsTouchingTwoDifferentPorts = true;
                            }
                        }
                    }
                    else
                    {
                        if (IsTouchingTwoDifferentPorts)
                        {
                            Debug.Log("Not touching two ports!");
                            ObjectRenderer.material.color = OriginalColor;
                            IsTouchingTwoDifferentPorts = false;
                        }
                    }
                }
            }
            else
            {
                if (ElementEnds[0])
                {
                    if (ElementEnds[0].CollidingPort)
                    {
                        if (!IsTouchingTwoDifferentPorts)
                        {
                            ObjectRenderer.material.color = Color.red;
                            IsTouchingTwoDifferentPorts = true;
                        }
                    }
                    else
                    {
                        if (IsTouchingTwoDifferentPorts)
                        {
                            Debug.Log("Not touching two ports!");
                            ObjectRenderer.material.color = OriginalColor;
                            IsTouchingTwoDifferentPorts = false;
                        }
                    }
                }
            }
        }
    }

    public override void OnStartClient()
    {
        // When we are spawned on the client,
        // find the parent object using its ID,
        // and set it to be our transform's parent.
        if (ElementType == 5)
        {
            // Wire
            ElementLineRenderer.SetPosition(0, LinePosition1);
            ElementLineRenderer.SetPosition(1, LinePosition2);
            GameObject parentObject = ClientScene.FindLocalObject(parentNetId);
            PlayerManager tmp_PlayerScript = parentObject.GetComponent<PlayerManager>();
            tmp_PlayerScript.WireElement = this;

            // Add collider to the line renderer
            WireCapsuleCollider.transform.position = (LinePosition1 + LinePosition2) / 2;
            float LineLength = Vector3.Distance(LinePosition2, LinePosition1) / WireCapsuleCollider.transform.localScale.y;
            WireCapsuleCollider.height = LineLength;
            WireCapsuleCollider.transform.LookAt(LinePosition2);
        }
        else
        {
            // Any other element
            OriginalColor = ObjectRenderer.material.color;
            GameObject parentObject = ClientScene.FindLocalObject(parentNetId);
            PlayerManager tmp_PlayerScript = parentObject.GetComponent<PlayerManager>();
            Debug.Log(tmp_PlayerScript.netId);
            if (tmp_PlayerScript.netId == CurrentPlayer.LocalPlayer.netId)
            {
                ChildrenObjects.SetActive(true);
                PhysicsObjectScript.PointerScript = tmp_PlayerScript.PointerScript;
                PhysicsObjectScript.SelectObject();
                PhysicsObjectScript.ElementYValue = this.transform.position.y;
                tmp_PlayerScript.AddedObject = this.gameObject;
                tmp_PlayerScript.ElementScript = this;
            }
        }
    }

    [Command]
    public void CmdRemoveElement()
    {
        TestScript.InstantiatedObject.GetComponent<CircuitBoard>().AddedElements.Remove(this);
    }

    [Command]
    public void CmdAddElementWire()
    {
        TestScript.InstantiatedObject.GetComponent<CircuitBoard>().AddedElements.Add(this);
    }

    [Command]
    public void CmdAddElementGround(int GroundedNode)
    {
        TestScript.InstantiatedObject.GetComponent<CircuitBoard>().AddedElements.Add(this);
        TestScript.InstantiatedObject.GetComponent<CircuitBoard>().GroundedNode = GroundedNode;
    }

    [Command]
    public void CmdAddElement(int NodeNumber1, int NodeNumber2)
    {
        PositiveNode = NodeNumber1;
        NegativeNode = NodeNumber2;
        TestScript.InstantiatedObject.GetComponent<CircuitBoard>().AddedElements.Add(this);
    }
}
