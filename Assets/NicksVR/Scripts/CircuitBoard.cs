﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CircuitBoard : NetworkBehaviour {

    // Element properties
    // Note, Node Array first contains nodes and then pos/neg rail 
    public CircuitNode[] NodeArray;
    public int NumberOfNodes;
    public NetlistCreator netlistscript;
    public List<CircuitElement> AddedElements;
    public int GroundedNode = 0;
    public bool IsValidSimulationOutput = false;


    public void Start()
    {
        AddedElements = new List<CircuitElement>();
    }

    public void ResetAllNodeVoltages(){
        for (int i = 1; i < NumberOfNodes; i++){
            RpcUpdateNodeVoltage(0, i);
        }
    }

    // Updates the node voltages. Function is run when the http request is finished
    public void UpdateNodeVoltages(string output)
    {
        ResetAllNodeVoltages();
        IsValidSimulationOutput = false;
        Debug.Log(output);
        string[] NodeVoltages = output.Split('\n');
        int CurrentNode = 0;
        for (int i = 0; i < NodeVoltages.Length; i++)
        {
            if (i == 0)
            {
                // Skip first line
            }
            else if (CustomStartsWith(NodeVoltages[i].Trim(), "<indep n") && NodeVoltages[i].Trim().Length > 0)
            {
                if(!IsValidSimulationOutput){
                    IsValidSimulationOutput = true;
                }
                int NodeNumber = int.Parse(NodeVoltages[i].Substring(8).Split('.')[0]);
                float VoltageValue = float.Parse(NodeVoltages[i + 1].Trim());
                RpcUpdateNodeVoltage(VoltageValue, NodeNumber);
            }
        }

        if(!IsValidSimulationOutput){
            // Network did not simulate properly - Return an error
        }
    }

    [ClientRpc]
    public void RpcUpdateNodeVoltage(float voltage, int NodeNumber){
        NodeArray[NodeNumber].Voltage = voltage;
    }

    public static bool CustomStartsWith(string a, string b)
    {
        int aLen = a.Length;
        int bLen = b.Length;
        int ap = 0; int bp = 0;

        while (ap < aLen && bp < bLen && a[ap] == b[bp])
        {
            ap++;
            bp++;
        }

        return (bp == bLen && aLen >= bLen) ||

                (ap == aLen && bLen >= aLen);
    }

}
