﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[Serializable]
public class CircuitNode : MonoBehaviour
{
    // Element properties
    public CircuitElementPort[] NodeElementPorts;

    [SerializeField]
    public List<CircuitElementPort> ConnectedElements; // Elements connected to the node

    // Node properies
    public CircuitBoard CircuitBoardScript;

    public int NodeNumber; // Number of the node

    // Synced variables
    public float Voltage; // Voltage of the node

    public bool IsGrounded = false;

    public CircuitNode()
    {
        ConnectedElements = new List<CircuitElementPort>(); // Initialise connected elements array
        Voltage = 0; // Initialise the voltage to 0
    }

    public void Start()
    {
        CircuitBoardScript = this.transform.parent.GetComponent<CircuitBoard>();
    }

    // Adds an element to the node
    public void AddElement(CircuitElementPort tmp_Element)
    {
            ConnectedElements.Add(tmp_Element);
    }

    // Removes an element from the node
    public void RemoveElement(CircuitElementPort tmp_Element)
    {
                ConnectedElements.Remove(tmp_Element);
    }
}
