﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_EDITOR
public class CircuitBoardSetUp : EditorWindow
{
#else
public class CircuitBoardSetUp : MonoBehaviour
{
#endif

    public static Shader BuildingShader;
    public static Color BuildingColour;


#if UNITY_EDITOR
    [MenuItem("Assets/CircuitBoard set up")]
    public static void ShowMaterialChanger()
    {
#if UNITY_EDITOR
        GetWindow<CircuitBoardSetUp>(true, "Circuit board set up").Repaint();
#endif
    }


    void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Add scripts"))
        {
            AddScripts();
        }


        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Fill in scripts"))
        {
            FillScripts();
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Remove all circuit scripts"))
        {
            RemoveAllCircuitScripts();
        }
        EditorGUILayout.EndHorizontal();
    }

    void RemoveAllCircuitScripts()
    {
        foreach (GameObject tmpParentObject in Selection.gameObjects)
        {
            foreach (Transform tmpChild in tmpParentObject.GetComponentsInChildren<Transform>())
            {

                if (tmpChild.gameObject.GetComponent<CircuitNode>())
                {
                    DestroyImmediate(tmpChild.gameObject.GetComponent<CircuitNode>());
                }
                else if (tmpChild.gameObject.GetComponent<CircuitElementPort>())
                {
                    DestroyImmediate(tmpChild.gameObject.GetComponent<CircuitElementPort>());
                }
            }
            if (tmpParentObject.gameObject.GetComponent<CircuitBoard>())
            {
                DestroyImmediate(tmpParentObject.GetComponent<CircuitBoard>());
            }
        }

    }


    void AddScripts()
    {
        foreach (GameObject tmpParentObject in Selection.gameObjects)
        {
            foreach (Transform tmpChild in tmpParentObject.GetComponentsInChildren<Transform>())
            {
                if (tmpChild.name.Contains("Node"))
                {
                    if (tmpChild.gameObject.GetComponent<CircuitNode>() == null)
                    {
                        tmpChild.gameObject.AddComponent<CircuitNode>();
                    }
                }
                else if (tmpChild.name.Contains("Port"))
                {
                    if (tmpChild.gameObject.GetComponent<CircuitElementPort>() == null)
                    {
                        tmpChild.gameObject.AddComponent<CircuitElementPort>();
                    }
                    tmpChild.GetComponentInChildren<BoxCollider>().isTrigger = true;
                }
                else if (tmpChild.name.Contains("PositiveRail"))
                {
                    if (tmpChild.gameObject.GetComponent<CircuitNode>() == null)
                    {
                        tmpChild.gameObject.AddComponent<CircuitNode>();
                    }
                }
                else if (tmpChild.name.Contains("NegativeRail"))
                {
                    if (tmpChild.gameObject.GetComponent<CircuitNode>() == null)
                    {
                        tmpChild.gameObject.AddComponent<CircuitNode>();
                    }
                }
            }
            if (tmpParentObject.gameObject.GetComponent<CircuitBoard>() == null)
            {
                tmpParentObject.gameObject.AddComponent<CircuitBoard>();
            }
        }
    }

    void FillScripts()
    {
        foreach (GameObject tmpParentObject in Selection.gameObjects)
        {
            CircuitBoard CircuitBoardScript = tmpParentObject.gameObject.GetComponent<CircuitBoard>();
            CircuitBoardScript.NodeArray = new CircuitNode[27];
            if (CircuitBoardScript != null)
            {
                // Alter values
            }
            foreach (Transform tmpChild in tmpParentObject.GetComponentsInChildren<Transform>())
            {
                if (tmpChild.name.Contains("Node"))
                {
                    CircuitNode NodeScript = tmpChild.gameObject.GetComponent<CircuitNode>();
                    if (NodeScript != null)
                    {
                        NodeScript.NodeElementPorts = new CircuitElementPort[5];
                        NodeScript.CircuitBoardScript = tmpParentObject.GetComponent<CircuitBoard>();
                        NodeScript.NodeNumber = int.Parse(tmpChild.name.Split('_')[1].Trim());
                        foreach (CircuitElementPort tmp_Port in NodeScript.GetComponentsInChildren<CircuitElementPort>())
                        {
                            NodeScript.NodeElementPorts[int.Parse(tmp_Port.name.Split('_')[1].Trim()) - 1] = tmp_Port;
                            tmp_Port.NodeNumber = NodeScript.NodeNumber;
                        }
                    }
                    CircuitBoardScript.NodeArray[NodeScript.NodeNumber] = NodeScript;

                }
                else if (tmpChild.name.Contains("Port"))
                {
                    CircuitElementPort ElementPortScript = tmpChild.gameObject.GetComponent<CircuitElementPort>();
                    if (ElementPortScript != null)
                    {
                        ElementPortScript.ParentNode = ElementPortScript.gameObject.transform.parent.GetComponent<CircuitNode>();
                        ElementPortScript.PortNumber = int.Parse(ElementPortScript.name.Split('_')[1].Trim());
                    }
                }
                else if (tmpChild.name.Contains("PositiveRail"))
                {
                    CircuitNode NodeScript = tmpChild.gameObject.GetComponent<CircuitNode>();
                    if (NodeScript != null)
                    {
                        NodeScript.NodeElementPorts = new CircuitElementPort[12];
                        NodeScript.CircuitBoardScript = tmpParentObject.GetComponent<CircuitBoard>();
                        foreach (CircuitElementPort tmp_Port in NodeScript.GetComponentsInChildren<CircuitElementPort>())
                        {
                            NodeScript.NodeElementPorts[int.Parse(tmp_Port.name.Split('_')[1].Trim()) - 1] = tmp_Port;
                            tmp_Port.NodeNumber = NodeScript.NodeNumber;
                        }
                    }
                    CircuitBoardScript.NodeArray[NodeScript.NodeNumber] = NodeScript;
                }
                else if (tmpChild.name.Contains("NegativeRail"))
                {
                    CircuitNode NodeScript = tmpChild.gameObject.GetComponent<CircuitNode>();
                    if (NodeScript != null)
                    {
                        NodeScript.NodeElementPorts = new CircuitElementPort[12];
                        NodeScript.CircuitBoardScript = tmpParentObject.GetComponent<CircuitBoard>();
                        foreach (CircuitElementPort tmp_Port in NodeScript.GetComponentsInChildren<CircuitElementPort>())
                        {
                            NodeScript.NodeElementPorts[int.Parse(tmp_Port.name.Split('_')[1].Trim()) - 1] = tmp_Port;
                        }
                    }
                    CircuitBoardScript.NodeArray[NodeScript.NodeNumber] = NodeScript;
                }
            }
        }
    }

#endif
}
