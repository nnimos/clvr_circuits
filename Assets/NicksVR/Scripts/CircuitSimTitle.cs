﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CircuitSimTitle : MonoBehaviour {
	public Button NonVRButton;
	public Button VRButton;

	public void NonVRButtonPress(){
		SceneManager.LoadSceneAsync("CircuitSimulator", LoadSceneMode.Single);
	}

	public void VRButtonPress()
    {

    }

	// Use this for initialization
	void Start () {
		
	}
}
