﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkScript : NetworkDiscovery {

    [SerializeField]
    public NetworkManager manager;

    private void Awake()
    {
        manager = GetComponent<NetworkManager>();
    }

    public void JoinServer(){
        Initialize();
        StartAsClient();
    }

    public void HostServer(){
        Initialize();
        manager.StartHost();
        StartAsServer();
    }


    public override void OnReceivedBroadcast(string fromAddress, string data)
    {
        // Address format NetworkManager:adr:port
        if(!manager.IsClientConnected()){
            manager.networkAddress = fromAddress;
            manager.StartClient();
            StopBroadcast();
        }
                
    }
}
