﻿// Modified/combined version of the laser pointer and manipulation scripts included with the Google VR SDK
//
// Note: The following lines of code are implemented at the start of each function. It is used to make sure
// that the function is only executed by the player that owns the script (Otherwise if one player interacts with
// a certain object, all the other players on the server will also interact with that object)
//
//if (PlayerNetworkBehaviour)
//{
//    if (!CurrentPlayer.LocalPlayer)
//    {
//        return;
//    }
//    else if (PlayerNetworkBehaviour.netId != CurrentPlayer.LocalPlayer.netId)
//    {
//        return;
//    }
//}

using UnityEngine;
using UnityEngine.EventSystems;
using DaydreamElements.ObjectManipulation;
using UnityEngine.Networking;
using System;

/// This class handles both manipulation of objects and interaction with menu items
[Serializable]
public class GvrAndManipulationPointer : GvrBasePointer
{
    /// Distance from the pointer that raycast hits will be detected.
    [Tooltip("Distance from the pointer that raycast hits will be detected.")]
    public float maxPointerDistance = 20.0f;

    /// Distance from the pointer that the reticle will be drawn at when hitting nothing.
    [Tooltip("Distance from the pointer that the reticle will be drawn at when hitting nothing.")]
    public float defaultReticleDistance = 20.0f;

    /// Distance from the pointer that the line will be drawn at when hitting nothing.
    [Tooltip("Distance from the pointer that the line will be drawn at when hitting nothing.")]
    public float defaultLineDistance = 0.3f;

    [Tooltip("By default, the length of the laser is used as the CameraRayIntersectionDistance. " +
    "Set this field to a non-zero value to override it.")]
    public float overrideCameraRayIntersectionDistance;

    /// The laser visual used by the pointer.
    [Tooltip("The laser visual used by the pointer.")]
    public FlexLaserVisual flexLaserVisual;

    /// The reticle used by the pointer.
    [Tooltip("The reticle used by the pointer.")]
    public FlexReticle reticle;

    // Physics layer mask to check if the physics raycast is hitting the corresponding object
    public LayerMask WireMask;
    public LayerMask ElementMask;
    public LayerMask BoardMask;

    /// The percentage of the reticle mesh that shows the reticle.
    /// The rest of the reticle mesh is transparent.
    private const float RETICLE_VISUAL_RATIO = 0.1f;

    // True if hitting the target
    private bool isHittingTarget;

    // Results of the physics raycasts
    public RaycastHit PortRaycastResult;
    public RaycastHit BoardRaycastResult;

    // Selected object transform and the point it was selected
    private static Transform selectedTransform;
    private static Vector3 selectedPoint;

    public GvrLaserVisual LaserVisual { get; private set; }

    // The corresponding player object that this script belongs to
    [SerializeField]
    public NetworkBehaviour PlayerNetworkBehaviour;

    // Assigns the grabbed object to the player
    public void SetSelected(Transform t, Vector3 localSpaceHitPosition)
    {
        if (PlayerNetworkBehaviour)
        {
            if (!CurrentPlayer.LocalPlayer)
            {
                return;
            }
            else if (PlayerNetworkBehaviour.netId != CurrentPlayer.LocalPlayer.netId)
            {
                return;
            }
        }
        selectedTransform = t;
        selectedPoint = localSpaceHitPosition;
    }

    // Releases the grabbed object
    public void ReleaseSelected(Transform t)
    {
        if (PlayerNetworkBehaviour)
        {
            if (!CurrentPlayer.LocalPlayer)
            {
                return;
            }
            else if (PlayerNetworkBehaviour.netId != CurrentPlayer.LocalPlayer.netId)
            {
                return;
            }
        }
        Debug.Log("Releasing");

        if (selectedTransform == t)
        {
            selectedTransform = null;
        }
    }

    // Checks if an object has been selected
    public bool IsObjectSelected()
    {
        return selectedTransform != null;
    }

    // Get the distance of the pointer
    public override float MaxPointerDistance
    {
        get
        {
            return maxPointerDistance;
        }
    }

    public override float CameraRayIntersectionDistance
    {
        get
        {
            if (overrideCameraRayIntersectionDistance != 0.0f)
            {
                return overrideCameraRayIntersectionDistance;
            }

            return LaserVisual != null ?
              LaserVisual.maxLaserDistance : overrideCameraRayIntersectionDistance;
        }
    }

    public override void GetPointerRadius(out float enterRadius, out float exitRadius)
    {
        if (LaserVisual.reticle != null)
        {
            float reticleScale = LaserVisual.reticle.transform.localScale.x;

            // Fixed size for enter radius to avoid flickering.
            // This will cause some slight variability based on the distance of the object
            // from the camera, and is optimized for the average case.
            enterRadius = LaserVisual.reticle.sizeMeters * 0.5f * RETICLE_VISUAL_RATIO;

            // Dynamic size for exit radius.
            // Always correct because we know the intersection point of the object and can
            // therefore use the correct radius based on the object's distance from the camera.
            exitRadius = reticleScale * LaserVisual.reticle.ReticleMeshSizeMeters * RETICLE_VISUAL_RATIO;
        }
        else
        {
            enterRadius = 0.0f;
            exitRadius = 0.0f;
        }
    }

    // Checks if a circuit element is still above the circuitboard while being dragged
    public bool CheckForBoardElement(GameObject CircuitElement)
    {
        Ray BoardRaycast = new Ray
        {
            origin = PointerTransform.position,
            direction = (CircuitElement.transform.position - PointerTransform.position).normalized
        };
        if (Physics.Raycast(BoardRaycast, out BoardRaycastResult, Mathf.Infinity, BoardMask))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // Checks if a circuitboard is at the end of the controller laser
    public bool CheckForBoard(Vector3 EndPoint)
    {
        Ray BoardRaycast = new Ray
        {
            origin = PointerTransform.position,
            direction = (EndPoint - PointerTransform.position).normalized
        };
        if (Physics.Raycast(BoardRaycast, out BoardRaycastResult, Mathf.Infinity, BoardMask))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // Checks if a circuit element exists at the end of the controller laser
    public bool CheckForElement(){
        Ray PortRaycast = new Ray
        {
            origin = PointerTransform.position,
            direction = (reticle.TargetPosition - PointerTransform.position).normalized
        };
        if (Physics.Raycast(PortRaycast, out PortRaycastResult, Mathf.Infinity, ElementMask))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // Checks if a circuit board port exists at the end of the controller laser
    public bool CheckForPort(){
        Ray PortRaycast = new Ray
        {
            origin = PointerTransform.position,
            direction = (reticle.TargetPosition - PointerTransform.position).normalized
        };
        if (Physics.Raycast(PortRaycast, out PortRaycastResult, Mathf.Infinity, WireMask)){
            return true;
        }
        else{
            return false;
        }
    }

    // Function is executed when the end of the controller laser is on top of an object
    public override void OnPointerEnter(RaycastResult raycastResult, bool isInteractive)
    {
        OnPointerHover(raycastResult, isInteractive);
        LaserVisual.SetDistance(raycastResult.distance);
        isHittingTarget = true;
    }

    // Function is executed when the controller laser is hovering over another object
    public override void OnPointerHover(RaycastResult raycastResult, bool isInteractive)
    {
        if (!IsObjectSelected())
        {
            reticle.SetTargetPosition(raycastResult.worldPosition, PointerTransform.position);
            Vector3 ray = raycastResult.worldPosition - PointerTransform.position;
            if (flexLaserVisual != null)
            {
                if (ray.magnitude < defaultLineDistance)
                {
                    flexLaserVisual.SetReticlePoint(raycastResult.worldPosition);
                }
                else
                {
                    SetLaserToDefaultDistance();
                }
            }
            LaserVisual.SetDistance(raycastResult.distance);
            isHittingTarget = true;
        }
    }

    // Function is executed when the controller laser moves away from an object
    public override void OnPointerExit(GameObject previousObject)
    {
        isHittingTarget = false;
    }

    public override void OnPointerClickDown()
    {

    }

    public override void OnPointerClickUp()
    {

    }

    // Activated when the player is activated
    void Awake()
    {
        LaserVisual = GetComponent<GvrLaserVisual>();
    }

    protected override void Start()
    {
        base.Start();
        LaserVisual.GetPointForDistanceFunction = GetPointAlongPointer;
        LaserVisual.SetDistance(defaultReticleDistance, true);
    }

    // Occurs once per frame and checks the status of the controller laser
    void Update()
    {
        if (PlayerNetworkBehaviour)
        {
            if (!CurrentPlayer.LocalPlayer)
            {
                return;
            }
            else if (PlayerNetworkBehaviour.netId != CurrentPlayer.LocalPlayer.netId)
            {
                return;
            }
        }
        if (!isHittingTarget)
        {
            if (!LaserVisual.IsLaserOn())
            {
                LaserVisual.TurnOnLaser();
            }
            reticle.SetTargetPosition(GetPointAlongPointer(defaultReticleDistance), PointerTransform.position);
            SetLaserToDefaultDistance();
        }
        if (IsObjectSelected())
        {
            if(LaserVisual.IsLaserOn()){
                LaserVisual.TurnOffLaser();
            }
            reticle.Hide();
            float dist = (selectedTransform.position - PointerTransform.position).magnitude;
            flexLaserVisual.SetReticlePoint(GetPointAlongPointer(dist));
        }
        else
        {
            if (!LaserVisual.IsLaserOn())
            {
                LaserVisual.TurnOnLaser();
            }
            reticle.Show();
        }
        LaserVisual.SetDistance(defaultReticleDistance);
        flexLaserVisual.UpdateVisual(selectedTransform, selectedPoint);
    }

    // Changes the laser length to stop at whatever object is hit
    private void SetLaserToDefaultDistance()
    {
        Vector3 direction = (reticle.TargetPosition - PointerTransform.position).normalized;
        Vector3 laserPoint = transform.position + (direction * defaultLineDistance);

        flexLaserVisual.SetReticlePoint(laserPoint);
    }
}
