﻿Shader "Nicks shaders/Satellite Shader" {
    Properties {
      _MainTex ("Texture", 2D) = "white" {}
	  _Color("Color",COLOR) = (0.5,0.5,0.5,1.0)
    }
    SubShader {
      Tags { "RenderType" = "Opaque" }
      CGPROGRAM
      #pragma surface surf Lambert

	  fixed4 _Color;

      struct Input {
          float2 uv_MainTex;
      };
      sampler2D _MainTex;
      void surf (Input IN, inout SurfaceOutput o) {
	  fixed4 c = fixed4(_Color);
          o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb * c.rgb;
      }
      ENDCG
    } 
    Fallback "Diffuse"
  }