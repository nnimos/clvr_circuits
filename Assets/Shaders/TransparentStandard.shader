﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.17 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.17;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:4013,x:32719,y:32712,varname:node_4013,prsc:2|diff-1304-RGB;n:type:ShaderForge.SFN_Color,id:1304,x:32443,y:32712,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;proporder:1304;pass:END;sub:END;*/

Shader "Nicks Shaders/Transparent shader" {
		Properties
	{
		_Color("Color",COLOR) = (0.5,0.5,0.5,1.0)
		 _Glossiness ("Smoothness", Range(0.000000,1.000000)) = 0.500000
		 _Metallic ("Metallic", Range(0,1)) = 0.0
	}

		SubShader
	{
		 Tags {"Queue" = "Transparent" "RenderType"="Transparent" }
		LOD 200
		CGPROGRAM
#pragma surface surf Standard fullforwardshadows alpha:fade

#pragma target 3.0
	half _Glossiness;
        half _Metallic;
        fixed4 _Color;

	struct Input
	{
		float2 uv_MainTex;
	};

	void surf(Input IN, inout SurfaceOutputStandard o)
	{
		fixed4 c = fixed4(_Color);
		o.Metallic = _Metallic;
		o.Albedo = c.rgb;
		o.Alpha = c.a;
		o.Smoothness = _Glossiness;
	}
	ENDCG
	}
		Fallback "Mobile/VertexLit"
}
